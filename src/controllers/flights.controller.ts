import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers';
import { FlightsService } from '../services/flights.service';
import { Flight } from '../databases/mongo/models/flights.model';

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService;

    constructor() {
        this._flightsService = new FlightsService();
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._flightsService.getFlights(),
        };
    }

    @Put('/:code')
    async updateFlightStatus(@Param('code') code: string) {
        return {
            status: 200,
            data: await this._flightsService.updateFlightStatus(code),
        };
    }

    @Post('/:code/passengers/:passengerId')
    async assignPassengerToFlight(@Param('code') code: string, @Param('passengerId') passId: string) {
        const data = await this._flightsService.assignPassengerToFlight(code, passId);
        return {
            status: 200,
            data
        };
    }

    // add flight
    @Post('')
    async addFlight(
        @Body()
        flight: Flight
    ) {
        const data = await this._flightsService.addFlight(flight);
        return {
            status: 200,
            data
        };
    }
}
