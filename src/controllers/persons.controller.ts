import {
  JsonController,
  Get,
  Param,
  Put,
  Post,
  Body,
} from 'routing-controllers';
import { Flight } from '../databases/mongo/models/flights.model';
import { PersonsService } from '../services/persons.service';
import { Person } from '../databases/mongo/models/person.model';

@JsonController('/persons', { transformResponse: false })
export default class PersonsController {
  private _personsService: PersonsService;

  constructor() {
      this._personsService = new PersonsService();
  }

  @Get('')
  async getAll() {
      return {
          status: 200,
          data: await this._personsService.getPersons(),
      };
  }

  @Post('')
  async addPerson(
      @Body()
      person: Person
  ) {
      const data = await this._personsService.addPerson(person);
      return {
          status: 201,
          data
      };
  }
}
