import { Flight } from "./mongo/models/flights.model";
import { Person } from "./mongo/models/person.model";

export abstract class Database {
    public static _instance: any;

    public static getInstance() {
        // subclass must implement this method
    }

    public async getFlights() {
        // subclass must implement this method
    }

    public async getPersons() {
        // subclass must implement this method
    }

    public async updateFlightStatus(code: string) {
        // subclass must implement this method
    }

    public abstract addFlight(flight: Flight): Promise<Flight>;

    public abstract addPerson(person: Person): Promise<Person>;

    public abstract assignPassengerToFlight(code: string, personId: string): Promise<Flight>;
}
