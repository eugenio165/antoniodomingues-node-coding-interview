import { IsString } from 'class-validator';
import mongoose, { ObjectId, Schema, SchemaType, SchemaTypes } from 'mongoose';

export class Flight {
    @IsString()
    code: string;

    origin: string;
    destination: string;
    status: string;
    // PersonsModel
    passengers: ObjectId[];
}

const schema = new Schema<Flight>(
    {
        code: { required: true, unique: true, type: String },
        origin: { required: true, type: String },
        destination: { required: true, type: String },
        status: String,
        passengers: { type: [mongoose.SchemaTypes.ObjectId], ref: 'Persons' }
    },
    { timestamps: true },
);

export const FlightsModel = mongoose.model('Flights', schema);
