import { Schema, model } from "mongoose";

export interface Person {
  name: string;
  email: string;
}


const schema = new Schema<Person>(
  {
    name: { required: true, type: String },
    email: { required: true, type: String }
  },
  { timestamps: true },
);

export const PersonsModel = model('Persons', schema);
