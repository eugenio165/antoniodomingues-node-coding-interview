import { Database } from '../database_abstract';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { Flight, FlightsModel } from './models/flights.model';
import { Person, PersonsModel } from './models/person.model';
import PersonsController from '../../controllers/persons.controller';

export class MongoStrategy extends Database {

    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const mongo = await MongoMemoryServer.create();
        const uri = mongo.getUri();

        const mongooseOpts = {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        };

        const flights = [
            {
                code: 'ABC-123',
                origin: 'EZE',
                destination: 'LDN',
                status: 'ACTIVE',
            },
            {
                code: 'XYZ-678',
                origin: 'CRC',
                destination: 'MIA',
                status: 'ACTIVE',
            },
        ];

        (async () => {
            await mongoose.connect(uri, mongooseOpts);
            await FlightsModel.create(flights);
        })();
    }

    public async getFlights() {
        return FlightsModel.find({});
    }

    public async getPersons() {
        return PersonsModel.find({});
    }

    public addFlight(flight: Flight) {
        return FlightsModel.create(flight);
    }

    public addPerson(person: Person): Promise<Person> {
        return PersonsModel.create(person);
    }

    public async assignPassengerToFlight(code: string, personId: string): Promise<Flight> {
        await FlightsModel.updateOne({ code }, { $push: { passengers: personId }});
        return FlightsModel.findOne({ code });
    }
}
