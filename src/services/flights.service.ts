import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';
import { Flight } from '../databases/mongo/models/flights.model';

export class FlightsService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async getFlights() {
        return this._db.getFlights();
    }

    public async updateFlightStatus(code: string) {
        return this._db.updateFlightStatus(code);
    }

    public async addFlight(flight: Flight) {
        return this._db.addFlight(flight);
    }

    public async assignPassengerToFlight(code: string, passengerId: string) {
        return this._db.assignPassengerToFlight(code, passengerId);
    }
}
