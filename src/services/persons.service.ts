import { DatabaseInstanceStrategy } from "../database";
import { Database } from "../databases/database_abstract";
import { Person } from "../databases/mongo/models/person.model";

export class PersonsService {

  private readonly _db: Database;

  constructor() {
    this._db = DatabaseInstanceStrategy.getInstance();
  }

  getPersons() {
    return this._db.getPersons();
  }

  addPerson(person: Person) {
    return this._db.addPerson(person);
  }

}